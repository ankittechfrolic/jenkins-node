const http = require('http');

const port = 3000;

const server = http.createServer(function(req, res) {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World This is first jenkins deployment\n');
});

server.listen(port, function() {
  console.log('Server running at http://localhost:' + port);
});